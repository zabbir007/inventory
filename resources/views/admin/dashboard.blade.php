@extends('layouts.app')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-lg-6">
            <select class="form-control" aria-label="Default select example" onchange="location = this.value;">
              <option selected>Select</option>
              <option value="{{url('purchase/list/latest')}}">Latest Purchase</option>
              <option value="{{url('order/list/latest')}}">Latest Sales</option>
            </select>
          </div>
        </div>
        <div class="row" style="margin-top: 2%;">
            <a href="{{url('customer/list')}}">
              <div class="col-lg-4 col-6" style="background-color: white;">
                  <!-- small box -->
                  <div class="small-box bg-info" style="background-color: #0077f5;">
                      <div class="inner" style="height: 90px;">
                          <p style="text-align: center;font-size: large;color: black;"><b>Sales center</b></p>
                      </div>
                      <div class="icon">
                           <i class="fa fa-users"></i>
                       </div>
                      <a href="{{url('customer/list')}}" class="small-box-footer" style="background-color: black;">More info <i class="fa fa-users"></i></a>
                  </div>
              </div>
            </a>
            <a href="{{url('item')}}">
              <!-- ./col -->
              <div class="col-lg-4 col-6" style="background-color: white;">
                  <!-- small box -->
                  <div class="small-box bg-success" style="background-color: #a456a1;">
                      <div class="inner" style="height: 90px;">
                          <p style="text-align: center; color: black;font-size: large;"><b>Items</b></p>
                      </div>
                      <div class="icon">
                           <i class="fa fa-cubes"></i>
                       </div>
                      <a href="{{url('item')}}" class="small-box-footer" style="background-color: black;">More info <i class="fa fa-cubes"></i></a>
                  </div>
              </div>
            <!-- ./col -->
            </a>
            <a href="{{url('order/list')}}">
              <div class="col-lg-4 col-6" style="background-color: white;">
                  <!-- small box -->
                  <div class="small-box bg-warning" style="background-color: #ff8350;">
                      <div class="inner" style="height: 90px;">
                          <p style="text-align: center;color: black;font-size: large;"><b>Sales</b></p>
                      </div>
                      <div class="icon">
                           <i class="fa fa-list-ul"></i>
                       </div>
                      <a href="{{url('order/list')}}" class="small-box-footer" style="background-color: black;">More info <i class="fa fa-list-ul"></i></a>
                  </div>
              </div>
              <!-- ./col -->
            </a>
        </div>
        <div class="row" style="margin-top: 1%;">
          <a href="{{url('supplier')}}">
            <div class="col-lg-4 col-6" style="background-color: white;">
                <!-- small box -->
                <div class="small-box bg-danger" style="background-color: #70aa44;">
                    <div class="inner" style="height: 90px;">
                        <p style="text-align: center;color: black;font-size: large;"><b>Suppliers</b></p>
                    </div>
                    <div class="icon">
                         <i class="fa fa-users"></i>
                     </div>
                    <a href="{{url('supplier')}}" class="small-box-footer" style="background-color: black;">More info <i class="fa fa-users"></i></a>
                </div>
            </div>
          </a>
          <a href="{{url('purchase/list')}}">
            <div class="col-lg-4 col-6" style="background-color: white;">
                <!-- small box -->
                <div class="small-box bg-info" style="background-color: #00aed0;">
                    <div class="inner" style="height: 90px;">
                        <p style="text-align: center;color: black;font-size: large;"><b>Purchase</b></p>
                    </div>
                    <div class="icon">
                         <i class="fa fa-shopping-cart"></i>
                     </div>
                    <a href="{{url('purchase/list')}}" class="small-box-footer" style="background-color: black;">More info <i class="fa fa-shopping-cart"></i></a>
                </div>
            </div>
          <!-- ./col -->
          </a>
          <a href="{{url('transfer/list')}}">
            <div class="col-lg-4 col-6" style="background-color: white;">
                <!-- small box -->
                <div class="small-box bg-success" style="background-color: #70aa44;">
                    <div class="inner" style="height: 90px;">
                        <p style="text-align: center;color: black;font-size: large;"><b>Stock Transfers</b></p>
                    </div>
                    <div class="icon">
                         <i class="fa fa-truck"></i>
                     </div>
                    <a href="{{url('transfer/list')}}" class="small-box-footer" style="background-color: black;">More info <i class="fa fa-truck"></i></a>
                </div>
            </div>
          </a>
          
          <!-- ./col -->
        </div>
        <div class="row">
            <a href="{{url('report/inventory-stock-on-hand')}}">
              <!-- ./col -->
              <div class="col-lg-4 col-6" style="background-color: white;">
                  <!-- small box -->
                  <div class="small-box bg-warning" style="background-color: #feb101;">
                      <div class="inner" style="height: 90px;">
                          <p style="text-align: center;color: black;font-size: large;"><b>Reports</b></p>
                      </div>
                      <div class="icon">
                           <i class="fa fa-bar-chart"></i>
                       </div>
                      <a href="{{url('report/inventory-stock-on-hand')}}" class="small-box-footer" style="background-color: black;">More info <i class="fa fa-bar-chart"></i></a>
                  </div>
              </div>
              <!-- ./col -->
            </a>
            <a href="{{url('company/setting')}}">
              <div class="col-lg-4 col-6" style="background-color: white;">
                  <!-- small box -->
                  <div class="small-box bg-danger" style="background-color: #a456a1;">
                      <div class="inner" style="height: 90px;">
                          <p style="text-align: center;color: black;font-size: large;"><b>Settings</b></p>
                      </div>
                      <div class="icon">
                           <i class="fa fa-gears"></i>
                       </div>
                      <a href="{{url('company/setting')}}" class="small-box-footer" style="background-color: black;">More info <i class="fa fa-gears"></i></a>
                  </div>
              </div>
              <!-- ./col -->
            </a>
        </div>
        

    </section>
@endsection
@section('js')
<script>
  $(function () {
 'use strict';
    var areaChartData = {
      labels: jQuery.parseJSON('{!! $date !!}'),
      datasets: [
        {
          label: "Sales(<?= Session::get('currency_symbol')?>)",
          fillColor: "rgba(66,155,206, 1)",
          strokeColor: "rgba(66,155,206, 1)",
          pointColor: "rgba(66,155,206, 1)",
          pointStrokeColor: "#429BCE",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(66,155,206, 1)",
          data: {{$sale}}
        },
        {
          label: "Cost(<?= Session::get('currency_symbol')?>)",
          fillColor: "rgba(255,105,84,1)",
          strokeColor: "rgba(255,105,84,1)",
          pointColor: "#F56954",
          pointStrokeColor: "rgba(255,105,84,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(255,105,84,1)",
          data: {{$cost}}
        },
        {
          label: "Profit(<?= Session::get('currency_symbol')?>)",
          fillColor: "rgba(47, 182, 40,0.9)",
          strokeColor: "rgba(47, 182, 40,0.8)",
          pointColor: "#2FB628",
          pointStrokeColor: "rgba(47, 182, 40,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(47, 182, 40,1)",
          data: {{$profit}}
        }        
      ]
    };

    var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };
    //-------------
    //- LINE CHART -
    //--------------
    var lineChartCanvas = $("#lineChart").get(0).getContext("2d");
    var lineChart = new Chart(lineChartCanvas);
    var lineChartOptions = areaChartOptions;
    lineChartOptions.datasetFill = false;
    lineChart.Line(areaChartData, lineChartOptions);
  });
</script>
@endsection