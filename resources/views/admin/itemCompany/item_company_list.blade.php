@extends('layouts.app')
@section('content')
    <!-- Main content -->
    <section class="content">

        <div class="row">
            <div class="col-md-3">
                @include('layouts.includes.sub_menu')
            </div>
            <!-- /.col -->
            <div class="col-md-9">

                <div class="box box-default">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="top-bar-title padding-bottom">Company</div>
                            </div>

                            <div class="col-md-3 top-right-btn">
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#add-category" class="btn btn-block btn-default btn-flat btn-border-orange"><span class="fa fa-plus"> &nbsp;</span>Add Company</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box">

                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th width="5%">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($itemCompanyData as $data)
                                <tr>
                                    <td>{{ $data->com_name }}</td>
                                    <td>
                                        <form method="POST" action="{{ url("delete-item-company/$data->id") }}" accept-charset="UTF-8" style="display:inline">
                                            {!! csrf_field() !!}
                                            <button title="Delete" class="btn btn-xs btn-danger" type="button" data-toggle="modal" data-target="#confirmDelete" data-title="Delete" data-message="Delete">
                                                <i class="glyphicon glyphicon-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

    </section>

    <div id="add-category" class="modal fade" role="dialog" style="display: none;">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Add New</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('save-item-company') }}" method="post" id="myform1" class="form-horizontal">
                        {!! csrf_field() !!}

                        <div class="form-group">
                            <label class="col-sm-3 control-label require" for="inputEmail3">Name</label>

                            <div class="col-sm-6">
                                <input type="text" placeholder="Name" class="form-control" name="com_name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="btn_save" class="col-sm-3 control-label"></label>
                            <div class="col-sm-6">
                                <button type="button" class="btn btn-info btn-flat" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary btn-flat">Submit</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>



    @include('layouts.includes.message_boxes')
@endsection

@section('js')
    <script type="text/javascript">



        $('.edit_size').on('click', function() {
            var id = $(this).attr("id");

            $.ajax({
                url: '{{ URL::to('edit-size')}}',
                data:{  // data that will be sent
                    id:id
                },
                type: 'POST',
                dataType: 'JSON',
                success: function (data) {

                    $('#name').val(data.size_type);
                    $('#cat_id').val(data.id);

                    $('#edit-size').modal();
                }
            });

        });

        $('#myform1').validate({
            rules: {
                description: {
                    required: true
                },
                dflt_units: {
                    required: true
                }
            }
        });

        $('#editCat').validate({
            rules: {
                description: {
                    required: true
                },
                dflt_units: {
                    required: true
                }
            }
        });
    </script>
@endsection