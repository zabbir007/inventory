@extends('layouts.app')
@section('content')
    <style>
        
        @media print {
            body * {
                visibility: hidden;
            }
            #newIoPrint {
                /*position: absolute;
                left: 0;*/
                /*top: 0px;*/
                margin-top: -120px;
            }
            #newIoPrint, #newIoPrint * {
                visibility: visible;
            }
            #newIoPrintNot{
                visibility: hidden;
            }
        }
        .zoom:hover {
            -ms-transform: scale(5.5); /* IE 9 */
            -webkit-transform: scale(5.5); /* Safari 3-8 */
            transform: scale(5.5);
        }
    </style>
    <!-- Main content -->
    <section class="content">
        <!-- Top Box-->
        <div class="box">
            <div class="box-body">
                <strong>{{$itemInfo->description}}</strong>
            </div>
        </div><!--Top Box End-->
        <!-- Default box -->
        <div class="box">

            <!-- Custom Tabs -->
            <div class="nav-tabs-custom" id="tabs">
                <ul class="nav nav-tabs">

                    <li class="<?= ($tab == 'item-info') ? 'active' : '' ?>"><a href="#tab_1" data-toggle="tab"
                                                                                aria-expanded="false">{{ trans('message.table.general_settings') }}</a>
                    </li>
                    <li class="<?= ($tab == 'sales-info') ? 'active' : '' ?>"><a href="#tab_2" data-toggle="tab"
                                                                                 aria-expanded="false">{{ trans('message.table.sales_pricing') }}</a>
                    </li>
                    <li class="<?= ($tab == 'purchase-info') ? 'active' : '' ?>"><a href="#tab_3" data-toggle="tab"
                                                                                    aria-expanded="true">{{ trans('message.table.purchase_pricing') }}</a>
                    </li>
                    
                    <li class="<?= ($tab == 'transaction-info') ? 'active' : '' ?>"><a href="#tab_4" data-toggle="tab"
                                                                                       aria-expanded="false">{{ trans('message.table.transctions') }}</a>
                    </li>
                    <li class="<?= ($tab == 'status-info') ? 'active' : '' ?>"><a href="#tab_5" data-toggle="tab"
                                                                                  aria-expanded="true">{{ trans('message.table.status') }}</a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane <?= ($tab == 'item-info') ? 'active' : '' ?>" id="tab_1">
                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="text-info text-center">{{ trans('message.table.item_info') }}</h4>
                                <form action="{{ url('update-item-info') }}" method="post" id="itemEditForm"
                                      class="form-horizontal" enctype="multipart/form-data">
                                    <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
                                    <input type="hidden" value="{{$itemInfo->id}}" name="id">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label require"
                                                   for="inputEmail3">{{ trans('message.form.item_id') }}</label>
                                            <div class="col-sm-9">
                                                <input type="text" placeholder="{{ trans('message.form.item_id') }}"
                                                       class="form-control" name="stock_id"
                                                       value="{{$itemInfo->stock_id}}" readonly="true">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                          <label class="col-sm-3 control-label" for="inputEmail3">Conpany Name</label>

                                          <div class="col-sm-9">
                                            <select class="form-control select2" name="company_name">
                                              <option value="">select</option>
                                                @foreach ($companies as $com)
                                                    <option value="{{$com->com_name}}" <?= ($itemInfo->company_name == $com->com_name) ? 'selected' : ''?>>{{$com->com_name}}</option>
                                                @endforeach
                                            </select>
                                          </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label require"
                                                   for="inputEmail3">{{ trans('message.form.item_name') }}</label>

                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="description"
                                                       value="{{$itemInfo->description}}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="inputEmail3">Lot Num</label>

                                            <div class="col-sm-9">
                                                <input type="text" placeholder="Lot Number" class="form-control"
                                                       name="lot_number" value="{{$itemInfo->lot_number}}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="inputEmail3">Size</label>
                                            <div class="col-sm-9">
                                                <select class="form-control select2" name="item_size" id="cat">
                                                    <option value="">select</option>
                                                    @foreach ($sizes as $size)
                                                    <option value="{{$size->size_type}}" <?= ($itemInfo->item_size == $size->size_type) ? 'selected' : ''?>>{{$size->size_type}} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                          @if($itemDesignName)
                                          <label class="col-sm-3 control-label" for="inputEmail3">Selected Design</label>
                                            <div class="col-sm-9">
                                              <select class="select2" multiple="multiple" data-placeholder="Select a name" disabled style="width: 100%;">
                                                @foreach ($itemDesignName as $itemDesignN)
                                                  <option selected="" value="{{$itemDesignN->name}}">{{$itemDesignN->name}}</option>
                                                @endforeach
                                              </select>
                                            </div>
                                          @endif
                                        </div>
                                        <div class="form-group">
                                          <label class="col-sm-3 control-label" for="inputEmail3">Design Name</label>

                                          <div class="col-sm-9">
                                            <select class="select2" name="design_name[]" multiple="multiple" data-placeholder="Select a name" style="width: 100%;">
                                                @foreach ($designName as $designN)
                                                <option value="{{$designN->des_name}}">{{$designN->des_name}}</option>
                                                @endforeach
                                            </select>
                                          </div>
                                        </div>
                                        

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="inputEmail3">Master</label>
                                            <div class="col-sm-9">
                                                <input type="text" placeholder="Master Name" class="form-control"
                                                       name="master_name" value="{{$itemInfo->master_name}}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="inputEmail3">Embroidery</label>
                                            <div class="col-sm-9">
                                                <input type="text" placeholder="Embroidery" class="form-control"
                                                       name="embroidery_name" value="{{$itemInfo->embroidery_name}}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"
                                                   for="inputEmail3">{{ trans('message.form.category') }}</label>
                                            <div class="col-sm-9">
                                                <select class="form-control select2" name="category_id">

                                                    @foreach ($categoryData as $data)
                                                        <option value="{{$data->category_id}}" <?= ($data->category_id == $itemInfo->category_id) ? 'selected' : ''?>>{{$data->description}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"
                                                   for="inputEmail3">{{ trans('message.form.item_tax_type') }}</label>
                                            <div class="col-sm-9">
                                                <select class="form-control select2" name="tax_type_id">

                                                    @foreach ($taxTypes as $taxType)
                                                        <option value="{{$taxType->id}}" <?= ($taxType->id == $itemInfo->tax_type_id) ? 'selected' : ''?>>{{$taxType->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"
                                                   for="inputEmail3">{{ trans('message.form.item_des') }}</label>

                                            <div class="col-sm-9">
                                                <textarea placeholder="{{ trans('message.form.item_des') }} ..."
                                                          rows="3" class="form-control"
                                                          name="long_description">{{$itemInfo->long_description}}</textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="inputEmail3">Total PC</label>
                                            <div class="col-sm-9">
                                                <input type="number" placeholder="Pc" class="form-control"
                                                       name="total_pc" value="{{$itemInfo->total_pc}}">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"
                                                   for="inputEmail3">{{ trans('message.form.status') }}</label>

                                            <div class="col-sm-9">
                                                <select class="form-control valdation_select" name="inactive">

                                                    <option value="0" <?=isset($itemInfo->inactive) && $itemInfo->inactive == 0 ? 'selected' : ""?> >
                                                        Active
                                                    </option>
                                                    <option value="1" <?=isset($itemInfo->inactive) && $itemInfo->inactive == 1 ? 'selected' : ""?> >
                                                        Inactive
                                                    </option>

                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"
                                                   for="inputEmail3">{{ trans('message.form.picture') }}</label>
                                            <div class="col-sm-9">
                                                <input type="file" class="form-control input-file-field"
                                                       name="item_image">
                                                <br>
                                                @if (!empty($itemInfo->item_image))
                                                    <img src='{{ url("public/uploads/itemPic/$itemInfo->item_image") }}' class="zoom"
                                                         alt="Item Image" width="80" height="80">
                                                @else
                                                    <img src='{{ url("public/uploads/default_product.jpg") }}' class="zoom"
                                                         alt="Item Image" width="80" height="80">
                                                @endif
                                                <input type="hidden" name="pic"
                                                       value="{{ $itemInfo->item_image ? $itemInfo->item_image : 'NULL' }}">

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"
                                                   for="inputEmail3">Create By</label>
                                            <div class="col-sm-9">
                                                <input type="text" readonly="" name="" value="{{$itemInfo->create_by}}">
                                            </div>
                                        </div>

                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer">
                                        <a href="{{ url('item') }}"
                                           class="btn btn-info btn-flat">{{ trans('message.form.cancel') }}</a>
                                        <button class="btn btn-primary pull-right btn-flat"
                                                type="submit">{{ trans('message.form.submit') }}</button>
                                    </div>

                                    <!-- /.box-footer -->
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane <?= ($tab == 'sales-info') ? 'active' : '' ?>" id="tab_2">
                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="text-info text-center"></h4>
                                <div class="box-body">

                                    <button data-toggle="modal" data-target="#add-type" type="button"
                                            class="btn btn-default add_br btn-flat btn-border-orange"
                                            style="margin-bottom: 10px;">{{ trans('message.table.add_new') }}</button>

                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>{{ trans('message.table.sale_type') }}</th>
                                            <th>{{ trans('message.table.price') }}({{Session::get('currency_symbol')}}
                                                )
                                            </th>
                                            <th width="15%" class="text-center">{{ trans('message.table.action') }}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $type = [1, 2];
                                        ?>
                                        @foreach($salePriceData as $value)
                                            <tr>
                                                <td>{{$salesTypeName[$value->sales_type_id]}}</td>
                                                <td>{{$value->price}}</td>
                                                <td class="text-center">

                                                    <button class="btn btn-xs btn-primary edit_type" id="{{$value->id}}"
                                                            type="button">
                                                        <i class="glyphicon glyphicon-edit"></i>
                                                    </button>&nbsp;

                                                    @if(! in_array($value->sales_type_id, $type))
                                                        <form method="POST"
                                                              action="{{ url("delete-sale-price/$value->id/$itemInfo->id") }}"
                                                              accept-charset="UTF-8" style="display:inline">
                                                            {!! csrf_field() !!}
                                                            <button title="{{ trans('message.form.Delete') }}"
                                                                    class="btn btn-xs btn-danger" type="button"
                                                                    data-toggle="modal" data-target="#confirmDelete"
                                                                    data-title="{{ trans('message.table.delete_item_header') }}"
                                                                    data-message="{{ trans('message.table.delete_item') }}">
                                                                <i class="glyphicon glyphicon-trash"></i>
                                                            </button> &nbsp;
                                                        </form>
                                                    @endif


                                                </td>
                                            </tr>
                                        @endforeach

                                        </tfoot>
                                    </table>

                                </div>


                                <div id="add-type" class="modal fade" role="dialog" style="display: none;">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">×</button>
                                                <h4 class="modal-title">{{ trans('message.table.add_new') }}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{ url('add-sale-price') }}" method="post"
                                                      id="salesInfoForm" class="form-horizontal">
                                                    <input type="hidden" value="{{csrf_token()}}" name="_token"
                                                           id="token">
                                                    <input type="hidden" value="{{$salesInfo->stock_id}}"
                                                           name="stock_id">
                                                    <input type="hidden" value="USD" name="curr_abrev" id="curr_abrev">
                                                    <input type="hidden" value="{{$itemInfo->id}}" name="item_id">

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label require"
                                                               for="inputEmail3">{{ trans('message.form.sales_type') }}</label>

                                                        <div class="col-sm-6">
                                                            <select class="form-control" name="sales_type_id">
                                                                <option value="">{{ trans('message.form.select_one') }}</option>
                                                                @foreach ($saleTypes as $saleType)
                                                                    <option value="{{$saleType->id}}">{{$saleType->sales_type}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label require"
                                                               for="inputEmail3">{{ trans('message.form.price') }}</label>

                                                        <div class="col-sm-6">
                                                            <input type="text" class="form-control" name="price"
                                                                   placeholder="{{ trans('message.form.price') }}">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="btn_save" class="col-sm-3 control-label"></label>
                                                        <div class="col-sm-6">
                                                            <button type="button" class="btn btn-info btn-flat"
                                                                    data-dismiss="modal">{{ trans('message.form.close') }}</button>
                                                            <button type="submit"
                                                                    class="btn btn-primary pull-right btn-flat">{{ trans('message.form.submit') }}</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <!-- edit modal sales type -->
                                <div id="edit-type-modal" class="modal fade" role="dialog" style="display: none;">
                                    <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">×</button>
                                                <h4 class="modal-title">{{ trans('message.table.edit') }}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{ url('update-sale-price') }}" method="post"
                                                      id="editType" class="form-horizontal">
                                                    {!! csrf_field() !!}

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label require"
                                                               for="inputEmail3">{{ trans('message.form.sales_type') }}</label>

                                                        <div class="col-sm-6">
                                                            <input type="text" id="sales_type_id" class="form-control"
                                                                   readonly>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label require"
                                                               for="inputEmail3">{{ trans('message.form.price') }}</label>

                                                        <div class="col-sm-6">
                                                            <input type="text" id="price" class="form-control"
                                                                   name="price"
                                                                   placeholder="{{ trans('message.form.price') }}">
                                                        </div>
                                                    </div>

                                                    <input type="hidden" name="id" id="type_id">
                                                    <input type="hidden" value="{{$itemInfo->id}}" name="item_id">


                                                    <div class="form-group">
                                                        <label for="btn_save" class="col-sm-3 control-label"></label>
                                                        <div class="col-sm-6">

                                                            <button type="button" class="btn btn-info btn-flat"
                                                                    data-dismiss="modal">{{ trans('message.form.close') }}</button>

                                                            <button type="submit"
                                                                    class="btn btn-primary btn-flat">{{ trans('message.form.submit') }}</button>


                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane <?= ($tab == 'purchase-info') ? 'active' : '' ?>" id="tab_3">
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="text-info text-center"></h4>
                                <div class="col-md-6">
                                    <form action="{{url('update-swing-section')}}" method="post">
                                        <label>Product swing section</label>
                                        <select name="swing" required="" class="form-control form-control-sm">
                                            <option value="0">Select</option>
                                            <option value="প্রসেস- ১">প্রসেস- ১</option>
                                            <option value="প্রসেস- ২">প্রসেস- ২</option>
                                            <option value="লোকাল সেকশন-">লোকাল সেকশন-</option>
                                            <option value="তাইফ তামিন- (বাড্ডা)">তাইফ তামিন- (বাড্ডা)</option>
                                            <option value="গুডফিল এ্যাপঃ (জুরাইন)">গুডফিল এ্যাপঃ (জুরাইন)</option>
                                            <option value="জি, এইচ, আই (নিকুঞ্জ)">জি, এইচ, আই (নিকুঞ্জ)</option>
                                            <option value="জুনায়েদ এ্যাপেঃ">জুনায়েদ এ্যাপেঃ</option>
                                        </select>
                                        <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
                                        <input type="hidden" value="{{$itemInfo->stock_id }}" name="stock_id">
                                        <input type="hidden" value="{{$itemInfo->id}}" name="item_id">
                                        <input type="submit" name="" style="margin-top: 3px;" value="Update" class="btn btn-info pull-right btn-flat">
                                    </form>
                                </div>
                                <form action="{{url('update-purchase-price')}}" method="post" id="purchaseInfoForm"
                                      class="form-horizontal">
                                    <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
                                    <input type="hidden" value="{{$itemInfo->stock_id }}" name="stock_id">
                                    <input type="hidden" value="{{$itemInfo->id}}" name="item_id">

                                    <div class="box-body newIo" id="newIo">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"
                                                   for="inputEmail3">{{ trans('message.form.price') }} <span
                                                        class="text-danger"> *</label>
                                            <div class="col-sm-12">
                                                <div id="newIoPrint">
                                                    <label id="newIoPrintNot">Add/Edit By: {{$purchaseInfo->create_by_price}}</label><br>
                                                    <label>Company Name: {{$itemInfo->company_name}}</label><br>
                                                    <label>Product Name: {{$itemInfo->description}}</label><br>
                                                    <label>Product Swing Section: {{$itemInfo->swing}}</label><br>
                                                    <label>Lot No: {{$itemInfo->lot_number}}</label><br>
                                                    <label>Size: {{$itemInfo->item_size}}</label><br>
                                                    <label>Quantity: {{$itemInfo->total_pc}}</label><br>

                                                @if($stock_master_purchase)
                                                    <table class="table table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th style="text-align: center;">Quantity</th>
                                                            <th style="text-align: center;">Units</th>
                                                            <th style="text-align: center;">Gross Rate</th>
                                                            <th style="text-align: center;">Gross Amount</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($stock_master_purchase as $stock_master)
                                                        <tr>
                                                            <td>
                                                                <span>{{$stock_master->name}}</span>
                                                            </td>
                                                            <td style="text-align: center;">
                                                                <span>{{$stock_master->quantity}}</span>
                                                            </td>
                                                            <td style="text-align: center;">
                                                                <span>{{$stock_master->unit}}</span>
                                                            </td>
                                                            <td style="text-align: center;">
                                                                <span>{{$stock_master->grossRate}}</span>
                                                            </td>
                                                            <td style="text-align: center;">
                                                                <span>{{$stock_master->grossCost}}</span>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <td style="text-align: center;" colspan="4">Sub-Total</td>
                                                            <td style="text-align: right;">{{isset($purchaseInfo->subTotal) ? $purchaseInfo->subTotal : 0}}
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: center;" colspan="4">Total PC</td>
                                                            <td style="text-align: right;">{{$itemInfo->total_pc}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: center;" colspan="4">Price</td>
                                                            <td style="text-align: right;">{{$purchaseInfo->price}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: center;" colspan="4">{{$otherCost->name}}</td>
                                                            <td style="text-align: right;">{{$otherCost->price}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: center;" colspan="4">Total Price</td>
                                                            <td style="text-align: right;">{{$purchaseInfo->price+$otherCost->price}}</td>
                                                        </tr>
                                                        <!-- <tr>
                                                            <td style="text-align: center;" colspan="4">Note</td>
                                                            <td style="text-align: right;">{{$purchaseInfo->pur_note}}</td>
                                                        </tr> -->

                                                        </tfoot>
                                                    </table>
                                                @endif
                                                
                                                <label>Master: {{$itemInfo->master_name}}</label>
                                                <br>
                                                <label>Embroidery: {{$itemInfo->embroidery_name}}</label>
                                                <br>
                                                <label>Note: {{$purchaseInfo->pur_note}}</label>
                                                </div>
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Quantity</th>
                                                        <th>Units</th>
                                                        <th>Gross Rate</th>
                                                        <th>Gross Amount</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr v-for="(invoice_product,k) in invoice_products" :key="k">
                                                        <td>
                                                            <select name="name[]" required="" v-model="invoice_product.name" class="form-control form-control-sm">
                                                                @foreach($clotheNameInfo as $clotheName)
                                                                <option value="{{$clotheName->clo_name}}">{{$clotheName->clo_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>

                                                        <td>
                                                            <input name="quantity[]" required="" 
                                                                   class="form-control form-control-sm" type="number"
                                                                   v-model="invoice_product.product_qty"
                                                                   @keyup="calculateLineTotal(invoice_product)"
                                                            />
                                                        </td>
                                                        <td>
                                                            <select name="unit[]" required="" v-model="invoice_product.unit" class="form-control form-control-sm">
                                                                @foreach ($unitData as $data)
                                                                    <option value="{{$data->name}}">{{$data->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input name="grossRate[]" required="" 
                                                                   class="form-control form-control-sm gross_rate" type="text"
                                                                   v-model="invoice_product.product_price" @keyup="calculateLineTotal(invoice_product)"/>
                                                        </td>
                                                        <td>
                                                            <input name="grossCost[]" readonly
                                                                   class="form-control form-control-sm"
                                                                   type="number" v-model="invoice_product.line_total"/>
                                                        </td>
                                                        <td class="text-center">
                                                            <button type='button' class="btn btn-info"
                                                                    @click="addNewRow" id="add_row_click_auto">
                                                                A
                                                            </button>
                                                            <button type='button' class="btn btn-info"
                                                                    @click="deleteRow(k,invoice_product)" id="modal">
                                                                D
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td style="text-align: center;" colspan="3">Sub-Total</td>
                                                            <td style="text-align: right;">@{{invoice_total}}
                                                                <input type="hidden" name="subTotal" v-model="invoice_total">
                                                            </td>

                                                        </tr>
                                                    </tfoot>
                                                </table>
                                                <input type="hidden" id="total_pc" value="{{$itemInfo->total_pc}}">
                                                <input type="text" readonly placeholder="{{ trans('message.form.price') }}"
                                                       class="form-control" v-model="pur_price" name="price">
                                                <br>
                                                <label>Purchase Note</label>
                                                <textarea placeholder="Purchase Note" rows="3" class="form-control" name="pur_note" spellcheck="false"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer">
                                        <a href="{{ url('item') }}"
                                           class="btn btn-primary btn-flat">{{ trans('message.form.cancel') }}</a>
                                        <button class="btn btn-info pull-right btn-flat"
                                                type="submit">{{ trans('message.form.submit') }}</button>
                                        <input type="button" onclick="printPage()" class="btn btn-success btn-sm btn-flat" value="Print">
                                    </div>

                                    <!-- /.box-footer -->
                                </form>
                            </div>
                        </div>

                    </div>

                    <!-- /.tab-pane -->
                    

                    <div style="min-height:200px" class="tab-pane <?= ($tab == 'transaction-info') ? 'active' : '' ?>"
                         id="tab_4">
                        @if(count($transations)>0)
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center">{{ trans('message.table.transaction_type')}}</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">{{ trans('message.table.transaction_date')}}</th>
                                    <th class="text-center">{{ trans('message.table.location')}}</th>
                                    <th class="text-center">{{ trans('message.table.qty_in')}}</th>
                                    <th class="text-center">{{ trans('message.table.qty_out')}}</th>
                                    <th class="text-center">{{ trans('message.table.qty_on_hand')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $sum = 0;
                                $StockIn = 0;
                                $StockOut = 0;
                                ?>
                                @foreach($transations as $result)
                                    <tr>
                                        <td align="center">

                                            @if($result->trans_type == PURCHINVOICE)
                                            <?php
                                                $loc=DB::table('purch_orders')
                                                    ->where('order_no', '=', $result->transaction_reference_id)
                                                    ->leftJoin('suppliers','suppliers.supplier_id','=','purch_orders.supplier_id')
                                                    ->select('suppliers.supp_name')
                                                    ->first();
                                            ?>
                                                <a href="{{URL::to('/purchase/view-purchase-details/'.$result->transaction_reference_id)}}">Purchase</a>
                                            @elseif($result->trans_type == SALESINVOICE)
                                            <?php
                                                $loc=DB::table('sales_orders')
                                                     ->where('sales_orders.order_no',$result->order_no)
                                                     ->leftjoin('debtors_master','debtors_master.debtor_no','=','sales_orders.debtor_no')
                                                     ->select('debtors_master.name')                            
                                                     ->first();
                                            ?>
                                                <a href="{{URL::to('/invoice/view-detail-invoice/'.$result->order_no.'/'.$result->transaction_reference_id)}}">Sale</a>
                                            @elseif($result->trans_type == STOCKMOVEIN)
                                                <a href="{{URL::to('/transfer/view-details/'.$result->transaction_reference_id)}}">Transfer</a>
                                            @elseif($result->trans_type == STOCKMOVEOUT)
                                                <a href="{{URL::to('/transfer/view-details/'.$result->transaction_reference_id)}}">Transfer</a>
                                            @endif

                                        </td>
                                        <td align="center">
                                            @if($result->trans_type == PURCHINVOICE)
                                            {{$loc->supp_name}}
                                            @elseif($result->trans_type == SALESINVOICE)
                                            {{$loc->name}}
                                            @else
                                            <?php echo "None"; ?>
                                            @endif
                                        </td>
                                        <td align="center">{{formatDate($result->tran_date)}}</td>
                                        <td align="center">{{$result->location_name}}</td>
                                        <td align="center">
                                            @if($result->qty >0 )
                                                {{$result->qty}}
                                                <?php
                                                $StockIn += $result->qty;
                                                ?>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td align="center">
                                            @if($result->qty <0 )
                                                {{str_ireplace('-','',$result->qty)}}
                                                <?php
                                                $StockOut += $result->qty;
                                                ?>
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td align="center">{{$sum += $result->qty}}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td colspan="4" align="right">{{ trans('message.table.total')}}</td>
                                    <td align="center">{{$StockIn}}</td>
                                    <td align="center">{{str_ireplace('-','',$StockOut)}}</td>
                                    <td align="center">{{$StockIn+$StockOut}}</td>
                                </tr>
                                </tbody>
                            </table>
                        @else
                            <br>
                            {{ trans('message.table.no_transaction')}}
                        @endif

                    </div>

                    <!-- /.tab-pane status -->
                    <div class="tab-pane <?= ($tab == 'status-info') ? 'active' : '' ?>" id="tab_5">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="box-body">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th>{{ trans('message.table.location')}}</th>
                                            <th>{{ trans('message.table.qty_available')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(!empty($locData))
                                            <?php
                                            $sum = 0;
                                            ?>
                                            @foreach ($locData as $data)
                                                <tr>
                                                    <td>{{$data->location_name}}</td>
                                                    <td>{{getItemQtyByLocationName($data->loc_code,$salesInfo->stock_id)}}</td>
                                                </tr>
                                                <?php
                                                $sum += getItemQtyByLocationName($data->loc_code, $salesInfo->stock_id);
                                                ?>
                                            @endforeach
                                        @endif
                                        <tr>
                                            <td align="right">{{ trans('message.invoice.total') }}</td>
                                            <td>{{$sum}}</td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->

        </div>
        <div class="clearfix"></div>
        <!-- /.box-footer-->

        <!-- /.box -->

    </section>

    @include('layouts.includes.message_boxes')


@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
// Item form validation
            $('#itemEditForm').validate({
                rules: {
                    stock_id: {
                        required: true
                    },
                    description: {
                        required: true
                    },
                    category_id: {
                        required: true
                    },
                    tax_type_id: {
                        required: true
                    }
                }
            });
            // Sales form validation
            $('#salesInfoForm').validate({
                rules: {
                    sales_type_id: {
                        required: true
                    },
                    price: {
                        required: true
                    }
                }
            });

            // Purchse form validation
            $('#purchaseInfoForm').validate({
                rules: {
                    supplier_id: {
                        required: true
                    },
                    price: {
                        required: true
                    },
                    suppliers_uom: {
                        required: true
                    }
                }
            });

            $(".select2").select2({
                width: '100%'
            });


            $('.edit_type').on('click', function () {

                var id = $(this).attr("id");

                $.ajax({
                    url: '{{ URL::to('edit-sale-price')}}',
                    data: {  // data that will be sent
                        id: id
                    },
                    type: 'POST',
                    dataType: 'JSON',
                    success: function (data) {

                        $('#sales_type_id').val(data.sales_type_id);
                        $('#price').val(data.price);
                        $('#type_id').val(data.id);

                        $('#edit-type-modal').modal();
                    }
                });

            });

        });

    </script>
    <script type="text/javascript">
        new Vue({
            el: '#newIo',
            data: {
                message: "jabbir",
                invoice_total: 0,
                pur_price:0,
                invoice_products: [{
                    name: '',
                    product_price: '',
                    unit:'',
                    product_qty: '',
                    line_total: 0
                }],
            },
            methods: {
                addNewRow() {
                    this.invoice_products.push({
                        name: '',
                        product_price: '',
                        unit:'',
                        product_qty: '',
                        line_total: 0
                    });
                },
                deleteRow(index, invoice_product) {
                    var idx = this.invoice_products.indexOf(invoice_product);
                    if (idx > -1) {
                        this.invoice_products.splice(idx, 1);
                    }
                    this.calculateTotal();
                },

                calculateTotal() {
                    var subtotal, total,total_pc,final_pur_price;
                    subtotal = this.invoice_products.reduce(function (sum, product) {
                        var lineTotal = parseFloat(product.line_total);
                        if (!isNaN(lineTotal)) {
                            return sum + lineTotal;
                        }
                    }, 0);
                    this.invoice_subtotal = subtotal.toFixed(2);
                    total = subtotal;
                    total = parseFloat(total);
                    total_pc=$("#total_pc").val();
                    final_pur_price=total/total_pc;
                    if (!isNaN(total)) {
                        this.invoice_total = total.toFixed(2);
                        this.pur_price=final_pur_price.toFixed(2);
                    } else {
                        this.invoice_total = '0.00';
                        this.pur_price='0.00';
                    }
                },
                calculateLineTotal(invoice_product) {
                    var total = parseFloat(invoice_product.product_price) * parseFloat(invoice_product.product_qty);
                    if (!isNaN(total)) {
                        invoice_product.line_total = total.toFixed(2);
                        this.total_value = 0;
                    }
                    this.calculateTotal();
                },
            },

            mounted: function () {
            }
        })
    </script>
    <script>
        function printPage() {
            window.print();
        }
    </script>
@endsection