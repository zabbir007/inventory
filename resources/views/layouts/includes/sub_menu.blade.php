<!-- Profile Image -->
<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">{{trans('message.header.general_setting')}}</h3>
  </div>
  <div class="box-body no-padding" style="display: block;">
    <ul class="nav nav-pills nav-stacked">

      <li {{ isset($list_menu) &&  $list_menu == 'category' ? 'class=active' : ''}} ><a href="{{ URL::to("item-category")}}">{{ trans('message.sidebar.item_category') }}</a></li>
      <li {{ isset($list_menu) &&  $list_menu == 'sub_cat' ? 'class=active' : ''}} ><a href="{{ URL::to("sub-cat")}}">Sub Cat</a></li>
      <li {{ isset($list_menu) &&  $list_menu == 'size' ? 'class=active' : ''}} ><a href="{{ URL::to("item-size")}}">Size</a></li>

      @if(Helpers::has_permission(Auth::user()->id, 'manage_unit'))
      <li {{ isset($list_menu) &&  $list_menu == 'unit' ? 'class=active' : ''}} ><a href="{{ URL::to("unit")}}">{{ trans('message.extra_text.units') }}</a></li>
      @endif

      @if(Helpers::has_permission(Auth::user()->id, 'manage_db_backup'))
      <li {{ isset($list_menu) &&  $list_menu == 'backup' ? 'class=active' : ''}}><a href="{{ URL::to("backup/list")}}">{{ trans('message.extra_text.db_backup') }}</a></li>
      @endif
      
      @if(Helpers::has_permission(Auth::user()->id, 'manage_email_setup'))
      <li {{ isset($list_menu) &&  $list_menu == 'email_setup' ? 'class=active' : ''}}><a href="{{ URL::to("email/setup")}}">{{ trans('message.extra_text.email_setup') }}</a></li>
      @endif
      <li {{ isset($list_menu) &&  $list_menu == 'other_cost' ? 'class=active' : ''}} ><a href="{{ URL::to("other-cost")}}">Other Cost</a></li>
      <li {{ isset($list_menu) &&  $list_menu == 'clothe_name' ? 'class=active' : ''}} ><a href="{{ URL::to("clothe-name")}}">Clothe Name</a></li>
      <li {{ isset($list_menu) &&  $list_menu == 'item_company' ? 'class=active' : ''}} ><a href="{{ URL::to("item-company")}}">Company</a></li>
      <li {{ isset($list_menu) &&  $list_menu == 'design_name' ? 'class=active' : ''}} ><a href="{{ URL::to("design-name")}}">Design Name</a></li>
    </ul>
  </div>
  <!-- /.box-body -->
</div>