<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Model\Category;
use session;
use Input;
use Excel;
use DB;
use Validator;
use App\Http\Start\Helpers;

class CategoryController extends Controller
{
    public function __construct(){
     /**
     * Set the database connection. reference app\helper.php
     */   
        //selectDatabase();
    }
    /**
     * Display a listing of the Category.
     *
     * @return Category List page view
     */
    public function index()
    {
        $data['access'] = \Session::all();
        //d($data['access'],1);
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'general';
        $data['list_menu'] = 'category';
        
        $data['categoryData'] = (new Category)->getAllCategory();
        $data['unitData'] = (new Category)->getAllUnits();
        
        return view('admin.category.category_list', $data);
    }

    public function sizeIndex()
    {
        $data['access'] = \Session::all();
        //d($data['access'],1);
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'general';
        $data['list_menu'] = 'size';

        $data['sizeData'] = DB::table('item_size')->get();
        return view('admin.size.size_list', $data);
    }
    public function otherCostIndex()
    {
        $data['access'] = \Session::all();
        //d($data['access'],1);
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'general';
        $data['list_menu'] = 'other_cost';

        $data['otherCostData'] = DB::table('other_cost')->get();
        return view('admin.otherCost.other_cost_list', $data);
    }
    public function clotheNameIndex()
    {
        $data['access'] = \Session::all();
        //d($data['access'],1);
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'general';
        $data['list_menu'] = 'clothe_name';

        $data['clotheNameData'] = DB::table('clothe_name')->get();
        return view('admin.clotheName.clothe_name_list', $data);
    }
    public function designNameIndex()
    {
        $data['access'] = \Session::all();
        //d($data['access'],1);
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'general';
        $data['list_menu'] = 'design_name';

        $data['designNameData'] = DB::table('design_name')->get();
        return view('admin.designName.design_name_list', $data);
    }
    public function subCatIndex()
    {
        $data['access'] = \Session::all();
        //d($data['access'],1);
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'general';
        $data['list_menu'] = 'sub_cat';

        $data['subCatData'] = DB::table('sub_cat')->get();
        $data['catData'] = DB::table('stock_category')->get();
        return view('admin.subCat.sub_cat_list', $data);
    }

    public function itemCompanyIndex()
    {
        $data['access'] = \Session::all();
        //d($data['access'],1);
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'general';
        $data['list_menu'] = 'item_company';

        $data['itemCompanyData'] = DB::table('companies')->get();
        return view('admin.itemCompany.item_company_list', $data);
    }

    /**
     * Show the form for creating a new Category.
     *
     * @return Category create page view
     */
    public function create()
    {
        $data['menu'] = 'category';
        
        $data['unitData'] = (new Category)->getAllUnits();
        
        return view('admin.category.category_add', $data);
    }

    /**
     * Store a newly created Category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return redirect Category List page view
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|min:3',
            'dflt_units' => 'required',
        ]);

        $data['dflt_units'] = $request->dflt_units;
        $data['description'] = $request->description;
        $data['created_at'] = date('Y-m-d H:i:s');

        $id = (new Category)->createCategory($data);

        if (!empty($id)){

            \Session::flash('success',trans('message.success.save_success'));
            return redirect()->intended('item-category');
        } else {

            return back()->withInput()->withErrors(['email' => "Invalid Request !"]);
        }
    }
    public function storeSize(Request $request)
    {
        $this->validate($request, [
            'size_type' => 'required'
        ]);

        $data['size_type'] = $request->size_type;

        $id = DB::table('item_size')->insert($data);

        if (!empty($id)){

            \Session::flash('success',('Insert Done'));
            return redirect()->intended('item-size');
        } else {

            return back()->withInput()->withErrors(['email' => "Invalid Request !"]);
        }
    }

    public function storeOtherCost(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required'
        ]);

        $data['name'] = $request->name;
        $data['price'] = $request->price;

        $id = DB::table('other_cost')->insert($data);

        if (!empty($id)){

            \Session::flash('success',('Insert Done'));
            return redirect()->intended('other-cost');
        } else {

            return back()->withInput()->withErrors(['email' => "Invalid Request !"]);
        }
    }

    public function storeClotheName(Request $request)
    {
        $this->validate($request, [
            'clo_name' => 'required',
        ]);

        $data['clo_name'] = $request->clo_name;
        $data['clo_company'] = $request->clo_company;

        $id = DB::table('clothe_name')->insert($data);

        if (!empty($id)){

            \Session::flash('success',('Insert Done'));
            return redirect()->intended('clothe-name');
        } else {

            return back()->withInput()->withErrors(['email' => "Invalid Request !"]);
        }
    }
    public function storeDesignName(Request $request)
    {
        $this->validate($request, [
            'des_name' => 'required',
        ]);

        $data['des_name'] = $request->des_name;

        $id = DB::table('design_name')->insert($data);

        if (!empty($id)){

            \Session::flash('success',('Insert Done'));
            return redirect()->intended('design-name');
        } else {

            return back()->withInput()->withErrors(['email' => "Invalid Request !"]);
        }
    }
    public function storeSubCat(Request $request)
    {
        $this->validate($request, [
            'cat_name' => 'required',
            'sub_cat_name' => 'required',
        ]);

        $data['cat_name'] = $request->cat_name;
        $data['sub_cat_name'] = $request->sub_cat_name;

        $id = DB::table('sub_cat')->insert($data);

        if (!empty($id)){

            \Session::flash('success',('Insert Done'));
            return redirect()->intended('sub-cat');
        } else {

            return back()->withInput()->withErrors(['email' => "Invalid Request !"]);
        }
    }

    public function storeItemCompany(Request $request)
    {
        $this->validate($request, [
            'com_name' => 'required',
        ]);

        $data['com_name'] = $request->com_name;

        $id = DB::table('companies')->insert($data);

        if (!empty($id)){

            \Session::flash('success',('Insert Done'));
            return redirect()->intended('item-company');
        } else {

            return back()->withInput()->withErrors(['email' => "Invalid Request !"]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified Category.
     *
     * @param  int  $id
     * @return Category edit page view
     */
    public function edit()
    {
        $id = $_POST['id'];

        $categoryData = (new Category)->getById($id);

        $return_arr['name'] = $categoryData->description;
        $return_arr['dflt_units'] = $categoryData->dflt_units;
        $return_arr['category_id'] = $categoryData->category_id;

        echo json_encode($return_arr);
    }
    public function editSize()
    {
        $id = $_POST['id'];

        $categoryData = DB::table('item_size')->where('id',$id)->first();

        $return_arr['size_type'] = $categoryData->size_type;
        $return_arr['id'] = $categoryData->id;

        echo json_encode($return_arr);
    }

    /**
     * Update the specified Category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return redirect Category List page view
     */
    public function update(Request $request)
    {    
        $this->validate($request, [
            'description' => 'required|min:3',
            'dflt_units' => 'required',
            'cat_id' => 'required',
        ]);

        $id = $request->cat_id;
        $data['dflt_units'] = $request->dflt_units;
        $data['description'] = $request->description;
        $data['updated_at'] = date('Y-m-d H:i:s');

        $id = (new Category)->updateCategory($id, $data);

        \Session::flash('success',trans('message.success.update_success'));
            return redirect()->intended('item-category');
    }

    /**
     * Remove the specified Category from storage.
     *
     * @param  int  $id
     * @return redirect Category List page view
     */
    public function destroy($id)
    {
        if (isset($id)) {
            $record = \DB::table('stock_category')->where('category_id', $id)->first();
            if ($record) {
                
                \DB::table('stock_category')->where('category_id', '=', $id)->delete();

                \Session::flash('success',trans('message.success.delete_success'));
                return redirect()->intended('item-category');
            }
        }
    }
    public function destroySize($id)
    {
        if (isset($id)) {
            $record = \DB::table('item_size')->where('id', $id)->first();
            if ($record) {

                \DB::table('item_size')->where('id', '=', $id)->delete();

                \Session::flash('success',('Delete Done'));
                return redirect()->intended('item-size');
            }
        }
    }

    public function destroyOtherCost($id)
    {
        if (isset($id)) {
            $record = \DB::table('other_cost')->where('id', $id)->first();
            if ($record) {

                \DB::table('other_cost')->where('id', '=', $id)->delete();

                \Session::flash('success',('Delete Done'));
                return redirect()->intended('other-cost');
            }
        }
    }
    public function destroyClotheName($id)
    {
        if (isset($id)) {
            $record = \DB::table('clothe_name')->where('id', $id)->first();
            if ($record) {

                \DB::table('clothe_name')->where('id', '=', $id)->delete();

                \Session::flash('success',('Delete Done'));
                return redirect()->intended('clothe-name');
            }
        }
    }
    public function destroyDesignName($id)
    {
        if (isset($id)) {
            $record = \DB::table('design_name')->where('id', $id)->first();
            if ($record) {

                \DB::table('design_name')->where('id', '=', $id)->delete();

                \Session::flash('success',('Delete Done'));
                return redirect()->intended('design-name');
            }
        }
    }
    public function destroySubCat($id)
    {
        if (isset($id)) {
            $record = \DB::table('sub_cat')->where('id', $id)->first();
            if ($record) {

                \DB::table('sub_cat')->where('id', '=', $id)->delete();

                \Session::flash('success',('Delete Done'));
                return redirect()->intended('sub-cat');
            }
        }
    }
    public function destroyItemCompany($id)
    {
        if (isset($id)) {
            $record = \DB::table('companies')->where('id', $id)->first();
            if ($record) {

                \DB::table('companies')->where('id', '=', $id)->delete();

                \Session::flash('success',('Delete Done'));
                return redirect()->intended('item-company');
            }
        }
    }
    public function downloadCsv($type)
    {
        if ($type == 'csv' ) {
            $categorydata = (new Category)->getAllCategory();
            foreach ($categorydata as $key => $value) {
                $data[$key]['Category Name'] = $value->description;
                $data[$key]['Unit'] = $value->name;
            }
        }

        if( $type == 'sample' ) {
            $data[0]['Category Name'] = 'Sample Data'; 
            $data[0]['Unit'] = 'Sample Data';
            $data[1]['Category Name'] = 'Sample Data'; 
            $data[1]['Unit'] = 'Sample Data';

            $type = 'csv';
        }

        return Excel::create('Category_sheet'.time().'', function($excel) use ($data) {
            
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
            
        })->download($type);
    }

    public function import()
    {
        $data['menu'] = 'category';
        $data['header'] = 'category';
        $data['breadcrumb'] = 'addCategory';
        
        return view('admin.category.category_import', $data);
    }

    public function importCsv(Request $request)
    {
        $file = $request->file('import_file');
        
        $validator = Validator::make(
            [
                'file'      => $file,
                'extension' => strtolower($file->getClientOriginalExtension()),
            ],
            [
                'file'          => 'required',
                'extension'      => 'required|in:csv',
            ]
        );

        if ($validator->fails()) {
            return back()->withErrors(['email' => "File type Invalid !"]);
        }

        if (Input::hasFile('import_file')) {
                $path = Input::file('import_file')->getRealPath();
                $data = Excel::load($path, function($reader) {
            })->get();

            $unit_id = array();
            $unit =  DB::table('item_unit')->get();

            foreach ($unit as $value) {
                $unit_id[$value->name] = $value->id;
            }

            
            if (!empty($data) && $data->count()) {

                foreach ($data as $key => $value) {
                    $cat[] = ['description' => $value->category_name, 'dflt_units' => $value->unit];
                }
                $input = array_map("unserialize", array_unique(array_map("serialize", $cat)));
                sort($input);
                
                foreach ($input as $key => $value) {
                    
                    $unitCount = DB::table('item_unit')->where('name','=',$value['dflt_units'])->count();
                    $catNameCount = DB::table('stock_category')->where('description','=',$value['description'])->count();
                    
                    if ($unitCount > 0 && $catNameCount <= 0) {

                        $insert[] = ['description' => $value['description'], 'dflt_units' => $unit_id[$value['dflt_units']]];
                    }
                }
                
                if (!empty($insert)) {
                    DB::table('stock_category')->insert($insert);
                    
                    \Session::flash('success',trans('message.success.save_success'));
                    return redirect()->intended('item-category');
                }
            }
        }
        return back();
    }
}
